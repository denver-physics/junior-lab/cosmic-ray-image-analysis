FROM gitpod/workspace-full

## Install libgl libraries, needed by opencv
## necessary because these libraries are not installed by conda
RUN sudo apt-get update && \
    sudo apt-get install -y libglu1-mesa

USER gitpod

## Create a packages directory
RUN mkdir /home/gitpod/packages

## Install python
RUN wget --quiet https://repo.anaconda.com/archive/Anaconda3-2019.10-Linux-x86_64.sh -O /home/gitpod/packages/anaconda.sh && \
    /bin/bash /home/gitpod/packages/anaconda.sh -b -p /home/gitpod/packages/anaconda3 && \
    rm /home/gitpod/packages/anaconda.sh && \
    echo ". /home/gitpod/packages/anaconda3/etc/profile.d/conda.sh" >> /home/gitpod/env-setup.sh

## Create the environment with opencv
## also update conda in case people try to install stuff 
## otherwise they'll get an alarming message to update conda
RUN . /home/gitpod/env-setup.sh && \
    conda update -n base -c defaults conda --yes && \
    conda create --name cosmics python=3.8 --yes && \
    conda activate cosmics && \
    conda install nb_conda_kernels conda-forge::jupyterlab matplotlib numpy conda-forge::opencv --yes && \
    echo "conda activate cosmics" >> /home/gitpod/env-setup.sh 

## Set up .bashrc so environment with opencv is automatically enabled
RUN cat /home/gitpod/env-setup.sh >> /home/gitpod/.bashrc

## Set up Jupyter to run without requiring authentication
## and allow cross-origin requests
ARG JUPYTER_CONFIG_FILE=/home/gitpod/.jupyter/jupyter_notebook_config.py
RUN . /home/gitpod/env-setup.sh && \
    jupyter notebook --generate-config && \
    sed -i 's/#c.NotebookApp.password .*/c.NotebookApp.password = '\'''\''/' ${JUPYTER_CONFIG_FILE} && \
    sed -i 's/#c\.NotebookApp\.token .*/c.NotebookApp.token = '\'''\''/' ${JUPYTER_CONFIG_FILE} && \
    sed -i 's/#c\.NotebookApp\.allow_origin .*/c.NotebookApp.allow_origin = '\''*'\''/' ${JUPYTER_CONFIG_FILE}