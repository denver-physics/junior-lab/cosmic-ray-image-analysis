[![Gitpod Ready-to-Code](https://img.shields.io/badge/Gitpod-Ready--to--Code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/denver-physics/junior-lab/cosmic-ray-image-analysis) 

# Cosmic Ray Image Analysis

This repository contains resources for cosmic ray image analysis.

## Get started

In the terminal, type the command `jupyter-lab`.  

This will open another browser tab with the files listed on the left.  
Double-click on `CosmicRayImageAnalysis.ipynb` to open a file with a sample image analysis.

## Asking questions

If you have any questions or notice any issues with this code, we'd love to know about it!

Please create an issue at [the cosmic ray image analysis repository](https://gitlab.com/denver-physics/junior-lab/cosmic-ray-image-analysis).

## Contributing to this code

If you would like to make changes or additions to this code, please request permission through gitlab or contact Amy Roberts with your gitlab username.

## Using this code for an assignment

If you would like to use this code as part of an assignment, you should fork this repository.

See [these instructions for forking a gitlab repository](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#creating-a-fork) for further instructions, and open an issue if you have any questions on how to work with this code.

## Local environment setup - Linux and Mac

The example analysis file `CosmicRayImageAnalysis.ipynb` uses the `opencv` package.  To run the file, you must set up an environment with opencv installed.

1. If you haven't already, install an Anaconda python distribution.
1. Run the following command to create an environment with the `opencv` package installed:

```
conda env create --name cosmics --file cosmics.yml
```

## Local environment setup - Windows

I don't recommend trying to set this environment up in Windows - the opengl library is required and must be installed seperately from conda and then linked in.

If you must use a Windows machine, use WSL or WSL2 and follow the linux instructions.

Note that you will need to install Anaconda on WSL and start the jupyter notebook from WSL.  
I still haven't figured out how to open JupyterLab from WSL2 so if you make that work please let me know!